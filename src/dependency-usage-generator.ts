import {
    WebresourceKey,
    IEntrypointWebresourceDependenciesDescriptor,
    IChunkGroupWebresourceDependenciesDescriptor,
    IWebresourceSubDependencyResult,
} from './chunk-analyzer';
import { deduplicate } from './utils';
import { uniqBy, merge } from 'lodash';

export interface IWebresourceDependencies {
    webresourceKey: WebresourceKey;
    dependencies: Array<WebresourceKey>;
}

interface IWebresourceIndirectDependencies {
    webresourceKey: WebresourceKey;
    result: Array<IWebresourceDependencies>;
}

interface IDependencyUser {
    [key: string]: Array<WebresourceKey>;
}

const getPotentialIndirectDependencies = (
    chunkGroup: IChunkGroupWebresourceDependenciesDescriptor,
    inheritedIndirectDependencies: Array<IWebresourceDependencies> = []
): Array<IWebresourceDependencies> => {
    const indirectDependenciesArrs = chunkGroup.result.map<IWebresourceDependencies>(dependency => {
        return {
            webresourceKey: dependency.webresourceKey,
            dependencies: [...dependency.js.resources, ...dependency.css.resources],
        };
    });

    const indirectDependencies = ([] as Array<IWebresourceDependencies>).concat(...indirectDependenciesArrs);
    return uniqBy([...indirectDependencies, ...inheritedIndirectDependencies], 'webresourceKey');
};

const getPotentialDirectDependencies = (
    chunkGroup: IChunkGroupWebresourceDependenciesDescriptor,
    inheritedDirectDependencies: Array<WebresourceKey> = []
): Array<WebresourceKey> => {
    const directDependencies = chunkGroup.result.map(dependency => dependency.webresourceKey);
    return deduplicate([...inheritedDirectDependencies, ...directDependencies]);
};

const extractSubdependenciesFromWebresourceResult = (
    webresourceResultSet: IWebresourceSubDependencyResult
): Array<WebresourceKey> => {
    return deduplicate([...webresourceResultSet.css.resources, ...webresourceResultSet.js.resources]);
};

const calculateDependencies = (
    webresourceResultSet: IWebresourceSubDependencyResult,
    potentialDirectDependencies: Array<WebresourceKey>
): IWebresourceDependencies => {
    const webResourceSubdependencies = extractSubdependenciesFromWebresourceResult(webresourceResultSet);

    const potentialDirectDependenciesWithoutSelf = potentialDirectDependencies.filter(
        key => key !== webresourceResultSet.webresourceKey
    );

    const dependencies = webResourceSubdependencies.filter(key => potentialDirectDependenciesWithoutSelf.includes(key));

    return {
        webresourceKey: webresourceResultSet.webresourceKey,
        dependencies,
    };
};

const calculateIndirectDepencies = (
    webresourceResultSet: IWebresourceSubDependencyResult,
    potentialIndirectDependencies: Array<IWebresourceDependencies>
): IWebresourceIndirectDependencies => {
    const webResourceSubdependencies = extractSubdependenciesFromWebresourceResult(webresourceResultSet);

    const potentialIndirectDependenciesWithoutSelf = potentialIndirectDependencies.filter(
        definition => definition.webresourceKey !== webresourceResultSet.webresourceKey
    );

    const indirectDependencyResult = webResourceSubdependencies.map(key => {
        const resourceKeyContainingIndirectDependency = potentialIndirectDependenciesWithoutSelf
            .filter(indirectDependencyDefinition => indirectDependencyDefinition.dependencies.includes(key))
            .map(indirectDependencyDefinition => indirectDependencyDefinition.webresourceKey);

        return {
            webresourceKey: key,
            dependencies: resourceKeyContainingIndirectDependency,
        };
    });

    return {
        webresourceKey: webresourceResultSet.webresourceKey,
        result: indirectDependencyResult.filter(result => result.dependencies.length !== 0),
    };
};

const getIndirectDependenciesOfDirectDependency = (
    webresourceDependencies: Array<IWebresourceDependencies>
): IDependencyUser => {
    return webresourceDependencies.reduce<IDependencyUser>((acc, webresourceDependency) => {
        webresourceDependency.dependencies.forEach(dependencyKey => {
            if (!acc[dependencyKey]) {
                acc[dependencyKey] = [];
            }

            acc[dependencyKey].push(webresourceDependency.webresourceKey);
        });

        return acc;
    }, {});
};

const getSharedIndirectDependenciesOfWebresource = (
    inderectWebresourceDepeneciesSet: Array<IWebresourceIndirectDependencies>
): Array<IDependencyUser> => {
    return inderectWebresourceDepeneciesSet.map(inderectWebresourceDepenecySet =>
        getIndirectDependenciesOfDirectDependency(inderectWebresourceDepenecySet.result)
    );
};

const getDirectDependenciesOfSharedIndirectDependency = (
    sharedIndirectDependenciesOfWebresource: IDependencyUser
): IDependencyUser => {
    return Object.keys(sharedIndirectDependenciesOfWebresource).reduce<IDependencyUser>((acc, dependencyKey) => {
        const sharedIndirectDependencyOfWebresource = sharedIndirectDependenciesOfWebresource[dependencyKey];

        for (const webresourceKey of sharedIndirectDependencyOfWebresource) {
            if (!acc[webresourceKey]) {
                acc[webresourceKey] = [];
            }

            acc[webresourceKey].push(dependencyKey);
        }

        return acc;
    }, {});
};

const getUniqDependenciesPerWebresource = (
    webresourceResultSet: IWebresourceSubDependencyResult,
    indirectDependenciesOfDirectDependency: Array<WebresourceKey>,
    directDependenciesOfSharedIndirectDependency: Array<WebresourceKey>
): IWebresourceDependencies => {
    const uniqueDependencies = extractSubdependenciesFromWebresourceResult(webresourceResultSet).filter(
        potentialUniqDependency => {
            if (indirectDependenciesOfDirectDependency.includes(potentialUniqDependency)) {
                return false;
            }

            if (directDependenciesOfSharedIndirectDependency.includes(potentialUniqDependency)) {
                return false;
            }

            if (potentialUniqDependency === webresourceResultSet.webresourceKey) {
                return false;
            }

            return true;
        }
    );

    return {
        webresourceKey: webresourceResultSet.webresourceKey,
        dependencies: uniqueDependencies,
    };
};

export interface IDepenendecyDictionary {
    directDependenciesPerWebresource: Array<IWebresourceDependencies>;
    directDependencyUsers: IDependencyUser;
    indirectDependenciesPerWebresource: Array<IWebresourceDependencies>;
    directDependencyToIndirectDependencyToOtherDirectUsers: Array<IWebresourceIndirectDependencies>;
    indirectDependencyUsers: IDependencyUser;
    uniqDependenciesPerWebresource: Array<IWebresourceDependencies>;
}

const extractData = (
    chunkGroup: IChunkGroupWebresourceDependenciesDescriptor,
    potentialDirectDependencies: Array<WebresourceKey> = [],
    potentialIndirectDependencies: Array<IWebresourceDependencies> = []
): IDepenendecyDictionary => {
    const directDependenciesPerWebresource: Array<IWebresourceDependencies> = chunkGroup.result
        .map(webresourceResultSet => calculateDependencies(webresourceResultSet, potentialDirectDependencies))
        .filter(webresource => webresource.dependencies.length);

    const indirectDependenciesPerWebresource: Array<IWebresourceDependencies> = chunkGroup.result
        .map(webresourceResultSet => {
            const potentialDependencies = ([] as Array<string>).concat(
                ...potentialIndirectDependencies
                    .filter(indirect => indirect.webresourceKey !== webresourceResultSet.webresourceKey)
                    .map(indirect => indirect.dependencies)
            );
            return calculateDependencies(webresourceResultSet, potentialDependencies);
        })
        .filter(webresource => webresource.dependencies.length);

    const directDependencyToIndirectDependencyToOtherDirectUsers: Array<
        IWebresourceIndirectDependencies
    > = chunkGroup.result.map(webresourceResultSet =>
        calculateIndirectDepencies(webresourceResultSet, potentialIndirectDependencies)
    );

    const directDependencyUsers: IDependencyUser = getIndirectDependenciesOfDirectDependency(
        directDependenciesPerWebresource
    );
    const sharedIndirectDependenciesOfWebresource = getSharedIndirectDependenciesOfWebresource(
        directDependencyToIndirectDependencyToOtherDirectUsers
    );

    const mergedSharedIndirectDependenciesPerWebresource = merge({}, ...sharedIndirectDependenciesOfWebresource);
    const directDependenciesOfSharedIndirectDependency: IDependencyUser = getDirectDependenciesOfSharedIndirectDependency(
        mergedSharedIndirectDependenciesPerWebresource
    );

    const uniqDependenciesPerWebresource: Array<IWebresourceDependencies> = chunkGroup.result.map(
        webresourceResultSet =>
            getUniqDependenciesPerWebresource(
                webresourceResultSet,
                Object.keys(directDependencyUsers),
                Object.keys(directDependenciesOfSharedIndirectDependency)
            )
    );

    return {
        directDependencyUsers: directDependencyUsers,
        indirectDependencyUsers: directDependenciesOfSharedIndirectDependency,
        directDependenciesPerWebresource: directDependenciesPerWebresource,
        indirectDependenciesPerWebresource: indirectDependenciesPerWebresource,
        uniqDependenciesPerWebresource: uniqDependenciesPerWebresource,
        directDependencyToIndirectDependencyToOtherDirectUsers: directDependencyToIndirectDependencyToOtherDirectUsers,
    };
};

export type DependencyDescriptor = Map<string, IChunkgroupResourceDependencies>;
export type EntryDependencyDescriptor = Map<string, IEntryResourceDependencies>;

export interface IChunkgroupResourceDependencies {
    name: string;
    result: IDepenendecyDictionary;
}

export interface IEntryResourceDependencies extends IChunkgroupResourceDependencies {
    async: DependencyDescriptor;
}

export const generateDependencyUsageMappings = (
    webresourceEntries: Array<IEntrypointWebresourceDependenciesDescriptor>
): EntryDependencyDescriptor => {
    return webresourceEntries
        .map(entry => {
            const directDependencies = getPotentialDirectDependencies(entry);
            const indirectDependencies = getPotentialIndirectDependencies(entry);

            return {
                name: entry.name,
                result: extractData(entry, directDependencies, indirectDependencies),
                async: entry.async
                    .map(asyncChunkGroup => {
                        const combinedDirectDependencies = getPotentialDirectDependencies(
                            asyncChunkGroup,
                            directDependencies
                        );
                        const combinedIndirectDependencies = getPotentialIndirectDependencies(
                            asyncChunkGroup,
                            indirectDependencies
                        );

                        return {
                            name: asyncChunkGroup.name,
                            result: extractData(
                                asyncChunkGroup,
                                combinedDirectDependencies,
                                combinedIndirectDependencies
                            ),
                        };
                    })
                    .reduce(
                        (acc, asyncDescriptor) => {
                            acc.set(asyncDescriptor.name, asyncDescriptor);
                            return acc;
                        },
                        new Map() as DependencyDescriptor
                    ),
            };
        })
        .reduce(
            (acc, entryDescriptor) => {
                acc.set(entryDescriptor.name, entryDescriptor);
                return acc;
            },
            new Map() as EntryDependencyDescriptor
        );
};
